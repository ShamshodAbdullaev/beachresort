import React, {Component} from 'react';
import {FaCocktail, FaHiking, FaShuttleVan, FaBeer} from "react-icons/fa";
import Title from './Title';

class Services extends Component {
    state={
      services: [
          {
              icon: <FaCocktail/>,
              title: "Free cocktails",
              infos: 'Lorem ipsum dolor sit amet consectetur ' +
                  'adipisicing elit. Magni, corporis!'
          },
          {
              icon: <FaHiking/>,
              title: "Endless Hiking",
              infos: 'Lorem ipsum dolor sit amet consectetur ' +
                  'adipisicing elit. Magni, corporis!'
          },
          {
              icon: <FaShuttleVan/>,
              title: "Free shuttle",
              infos: 'Lorem ipsum dolor sit amet consectetur ' +
                  'adipisicing elit. Magni, corporis!'
          },
          {
              icon: <FaBeer/>,
              title: "Strongest Beer",
              infos: 'Lorem ipsum dolor sit amet consectetur ' +
                  'adipisicing elit. Magni, corporis!'
          }
      ]
    };
    render() {
        return (
            <section className="services">
                <Title title="services"/>
                <div className="services-center">
                    {this.state.services.map((item, index)=>{
                        return <article key={index} className="service">
                            <span>{item.icon}</span>
                            <h6>{item.title}</h6>
                            <p>{item.infos}</p>
                        </article>
                    })}
                </div>
            </section>
        );
    }
}

export default Services;